package friscilla.lely.appstokhanahijab

import android.app.AlertDialog
import android.content.DialogInterface
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.CursorAdapter
import android.widget.ListAdapter
import android.widget.SimpleCursorAdapter
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.frag_data_kat.*
import kotlinx.android.synthetic.main.frag_data_kat.view.*
import kotlinx.android.synthetic.main.frag_data_merk.*
import kotlinx.android.synthetic.main.frag_data_merk.view.*
import kotlinx.android.synthetic.main.frag_data_merk.view.spinner

class FragmentMerk : Fragment(), View.OnClickListener, AdapterView.OnItemSelectedListener {

    lateinit var thisParent: MainActivity
    lateinit var lsAdapter : ListAdapter
    lateinit var spAdapter: SimpleCursorAdapter
    lateinit var dialog : AlertDialog.Builder
    lateinit var v : View
    var namaMerk : String=""
    var namaKat : String=""
    lateinit var db : SQLiteDatabase

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        thisParent = activity as MainActivity
        v = inflater.inflate(R.layout.frag_data_merk,container,false)
        db = thisParent.getDbObject()
        dialog = AlertDialog.Builder(thisParent)
        v.btnDeleteMerk.setOnClickListener(this)
        v.btnInsertMerk.setOnClickListener(this)
        v.btnUpdateMerk.setOnClickListener(this)
        v.spinner.onItemSelectedListener = this
        v.btnCari.setOnClickListener(this)
        v.lsMerk.setOnItemClickListener(itemClick)

        return v
    }
    override fun onStart() {
        super.onStart()
        showDataMerk("")
        showDataKat()
    }
    val itemClick = AdapterView.OnItemClickListener{ parent, view, position, id ->
        val c: Cursor = parent.adapter.getItem(position) as Cursor
        namaMerk = c.getString(c.getColumnIndex("_id"))
        v.edNamaMerk.setText(c.getString(c.getColumnIndex("nama_merk")))
    }

    fun showDataMerk(namaMerk: String){
        var sql =""
        if(!namaMerk.trim().equals("")){
            sql = "select m.id_merk as _id, m.nama_merk, k.nama_kat from merk m, kategori k " +
                    "where m.id_kat=k.id_kat and m.nama_merk like '%$namaMerk%'"
        } else{
            sql = "select m.id_merk as _id, m.nama_merk, k.nama_kat from merk m, kategori k " +
                    "where m.id_kat = k.id_kat order by m.nama_merk asc"
        }

        val c : Cursor = db.rawQuery(sql,null)
        lsAdapter = SimpleCursorAdapter(thisParent,R.layout.item_data_merk,c,
            arrayOf("_id","nama_merk","nama_kat"), intArrayOf(R.id.txIdMerk,R.id.txNamaMerk, R.id.txNamaKat),
            CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        v.lsMerk.adapter = lsAdapter
    }
    fun showDataKat(){
        val c : Cursor = db.rawQuery("select nama_kat as _id from kategori order by nama_kat asc", null)
        spAdapter = SimpleCursorAdapter(thisParent, android.R.layout.simple_spinner_item,c,
            arrayOf("_id"), intArrayOf(android.R.id.text1),CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        spAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        v.spinner.adapter = spAdapter
        v.spinner.setSelection(0)
    }
    fun insertDataMerk(id_merk : String , namaMerk: String, id_kat : Int) {
        var sql = "insert into merk (id_merk,nama_merk,id_kat) values (?,?,?)"
        db.execSQL(sql, arrayOf(id_merk, namaMerk, id_kat))
        showDataMerk("")
    }
    val btnInsertDialog = DialogInterface.OnClickListener { dialog, which ->
        var sql = "select id_kat from kategori where nama_kat ='$namaKat'"
        val c: Cursor = db.rawQuery(sql, null)
        if (c.count > 0) {
            c.moveToFirst()
            insertDataMerk(
                v.edIdMerk.text.toString(), v.edNamaMerk.text.toString(),
                c.getInt(c.getColumnIndex("id_kat"))
            )
            v.edIdMerk.setText("")
            v.edNamaMerk.setText("")
        }
    }

    fun deleteDataMerk(id_merk: String  ){
        var sql = "delete from merk where id_merk = $id_merk"
        showDataMerk("")
    }

    val btnDeleteDialog = DialogInterface.OnClickListener { dialog, which ->
        deleteDataMerk(id_merk = "")
        v.edNamaMerk.setText("")
    }
    val btnUpdateDialog = DialogInterface.OnClickListener { dialog, which ->
        deleteDataMerk(id_merk = "")
        v.edNamaMerk.setText("")
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnDeleteMerk->{
                dialog.setTitle("Konfirmasi").setMessage("Yakin menghapus data ini?")
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setPositiveButton("Ya",btnDeleteDialog)
                    .setNegativeButton("Tidak",null)
                dialog.show()
            }
            R.id.btnUpdateMerk->{
                dialog.setTitle("Konfirmasi").setMessage("Yakin mengupdate data ini?")
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setPositiveButton("Ya",btnUpdateDialog)
                    .setNegativeButton("Tidak",null)
                dialog.show()
            }
            R.id.btnInsertMerk->{
                dialog.setTitle("Konfirmasi").setMessage("Data yang akan dimasukkan sudah benar?")
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setPositiveButton("Ya",btnInsertDialog)
                    .setNegativeButton("Tidak",null)
                dialog.show()
            }
            R.id.btnCari ->{
                showDataMerk(edNamaMerk.text.toString())
            }
        }
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
        spinner.setSelection(0, true)
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        val c : Cursor = spAdapter.getItem(position) as Cursor
        namaKat = c.getString(c.getColumnIndex("_id"))
    }}


