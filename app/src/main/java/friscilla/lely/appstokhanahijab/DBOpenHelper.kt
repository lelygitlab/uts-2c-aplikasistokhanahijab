package friscilla.lely.appstokhanahijab

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class DBOpenHelper(context : Context):SQLiteOpenHelper(context,DB_Name, null,DB_Ver) {
    companion object {
        val DB_Name = "merk"
        val DB_Ver = 1
    }

    override fun onCreate(db: SQLiteDatabase?) {
        val tMerk =
            "create table merk(id_merk text primary key, nama_merk text not null, id_kat int not null)"
        val tWarna =
            "create table warna(kdwarna text primary key, nama_warna text not null, id_kat int not null)"
        val tKat =
            "create table kategori(id_kat integer primary key autoincrement, nama_kat text not null)"
        val tStok =
            "create table stok(id_stok integer primary key autoincrement,id_merk text not null, kdwarna text not null, stok text not null)"
        val insKat = "insert into kategori(nama_kat) values('Instan'),('Square'),('Pashmina')"
        db?.execSQL(tMerk)
        db?.execSQL(tWarna)
        db?.execSQL(tKat)
        db?.execSQL(tStok)
        db?.execSQL(insKat)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {

    }
}