package friscilla.lely.appstokhanahijab

import android.app.AlertDialog
import android.content.ContentValues
import android.content.DialogInterface
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.frag_data_kat.view.*

class FragmentKat() : Fragment(),View.OnClickListener {

    lateinit var thisParent: MainActivity
    lateinit var db : SQLiteDatabase
    lateinit var adapter : ListAdapter
    lateinit var v : View
    lateinit var builder : AlertDialog.Builder
    var idKat : String = ""

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        thisParent = activity as MainActivity
        db = thisParent.getDbObject()

        v = inflater.inflate(R.layout.frag_data_kat,container,false)
        v.btnUpdate.setOnClickListener(this)
        v.btnInsert.setOnClickListener(this)
        v.btnDelete.setOnClickListener(this)
        builder = AlertDialog.Builder(thisParent)
        v.lsKat.setOnItemClickListener(itemClick)

        return v
    }

    fun showDataKat(){
        val cursor : Cursor = db.query("kategori", arrayOf("nama_kat", "id_kat as _id"),
            null, null, null,null,"nama_kat asc")
        adapter = SimpleCursorAdapter(thisParent,R.layout.item_data_kat,cursor,
            arrayOf("_id","nama_kat"), intArrayOf(R.id.txldKat, R.id.txNamaKategori),
            CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        v.lsKat.adapter = adapter
    }

    override fun onStart() {
        super.onStart()
        showDataKat()
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnInsert ->{
                builder.setTitle("Konfirmasi").setMessage("Data yang akan dimasukkan sudah benar?")
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setPositiveButton("Ya",btnInsertDialog)
                    .setNegativeButton("Tidak",null)
                builder.show()
            }
            R.id.btnDelete ->{
                builder.setTitle("Konfirmasi").setMessage("Yakin akan menghapus data ini?")
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setPositiveButton("Ya",btnDeleteDialog)
                    .setNegativeButton("Tidak",null)
                builder.show()
            }
            R.id.btnUpdate ->{
                builder.setTitle("Konfirmasi").setMessage("Data yang akan dimasukkan sudah benar?")
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setPositiveButton("Ya",btnUpdateDialog)
                    .setNegativeButton("Tidak",null)
                builder.show()
            }
        }
    }

    val itemClick = AdapterView.OnItemClickListener{ parent, view, position, id ->
        val c: Cursor = parent.adapter.getItem(position) as Cursor
        idKat = c.getString(c.getColumnIndex("_id"))
        v.edNamaKategori.setText(c.getString(c.getColumnIndex("nama_kat")))
    }

    fun insertDataKategori(namaKategori : String){
        var cv : ContentValues = ContentValues()
        cv.put("nama_kat",namaKategori)
        db.insert("kategori",null,cv)
        showDataKat()
    }
    fun updateDataKategori(namaKategori: String, idKategori: String){
        var cv : ContentValues = ContentValues()
        cv.put("nama_kat", namaKategori)
        db.update("kategori",cv,"id_kat = $idKategori",null)
        showDataKat()
    }
    fun deleteDataKategori(idKategori: String ){
        db.delete("kategori","id_kat = $idKategori", null)
        showDataKat()
    }

    val btnInsertDialog = DialogInterface.OnClickListener { dialog, which ->
        insertDataKategori(v.edNamaKategori.text.toString())
        v.edNamaKategori.setText("")
    }
    val btnUpdateDialog = DialogInterface.OnClickListener { dialog, which ->
        updateDataKategori(v.edNamaKategori.text.toString(), idKat)
        v.edNamaKategori.setText("")
    }
    val btnDeleteDialog = DialogInterface.OnClickListener { dialog, which ->
        deleteDataKategori(idKat)
        v.edNamaKategori.setText("")
    }
}