package friscilla.lely.appstokhanahijab

import android.database.sqlite.SQLiteDatabase
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.fragment.app.FragmentTransaction
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), BottomNavigationView.OnNavigationItemSelectedListener {

    lateinit var db : SQLiteDatabase
    lateinit var fragKat : FragmentKat
    lateinit var fragWarna : FragmentWarna
    lateinit var fragMerk : FragmentMerk
    lateinit var ft : FragmentTransaction

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        bottomNavigationView.setOnNavigationItemSelectedListener(this)
        fragKat = FragmentKat()
        fragMerk = FragmentMerk()
        fragWarna = FragmentWarna()

        db = DBOpenHelper(this).writableDatabase
    }

    fun getDbObject() : SQLiteDatabase{
        return db
    }

    override fun onNavigationItemSelected(p0: MenuItem): Boolean {
        when (p0.itemId) {
            R.id.itemKategori -> {
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.frameLayout, fragKat).commit()
                frameLayout.setBackgroundColor(Color.argb(245, 255, 255, 255))
                frameLayout.visibility = View.VISIBLE
            }
            R.id.itemMerk -> {
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.frameLayout, fragMerk).commit()
                frameLayout.setBackgroundColor(Color.argb(245, 255, 255, 255))
                frameLayout.visibility = View.VISIBLE
            }
            R.id.itemWarna ->{
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.frameLayout,fragWarna).commit()
                frameLayout.setBackgroundColor(Color.argb(245,255,255,255))
                frameLayout.visibility = View.VISIBLE
            }
            R.id.itemHome-> frameLayout.visibility = View.GONE
        }
        return true
    }
}

