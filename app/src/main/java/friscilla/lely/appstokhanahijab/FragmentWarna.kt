package friscilla.lely.appstokhanahijab

import android.app.AlertDialog
import android.content.ContentValues
import android.content.DialogInterface
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.frag_data_warna.*
import kotlinx.android.synthetic.main.frag_data_warna.view.*
import kotlinx.android.synthetic.main.frag_data_warna.view.spinner


class FragmentWarna : Fragment(), View.OnClickListener, AdapterView.OnItemSelectedListener {
    override fun onNothingSelected(parent: AdapterView<*>?) {
        spinner.setSelection(0, true)
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        val c : Cursor = spAdapter.getItem(position) as Cursor
        namaKategori = c.getString(c.getColumnIndex("_id"))
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnDeleteWarna->{
                dialog.setTitle("Konfirmasi").setIcon(android.R.drawable.ic_dialog_info)
                    .setMessage("Apakah data yang akan dimasukkan sudah benar?")
                    .setPositiveButton("Ya",btnDeleteDialog)
                    .setNegativeButton("Tidak",null)
                dialog.show()
            }
            R.id.btnUpdateWarna->{
                dialog.setTitle("Konfirmasi").setIcon(android.R.drawable.ic_dialog_info)
                    .setMessage("Apakah data yang akan dimasukkan sudah benar?")
                    .setPositiveButton("Ya",btnUpdateDialog)
                    .setNegativeButton("Tidak",null)
                dialog.show()
            }
            R.id.btnInsertWarna ->{
                dialog.setTitle("Konfirmasi").setMessage("Data yang akan dimasukkan sudah benar?")
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setPositiveButton("Ya",btnInsertDialog)
                    .setNegativeButton("Tidak",null)
                dialog.show()

            }
//            R.id.btnCari ->{
//                showDataMatkul(edNamaMatkul.text.toString())
//            }
        }
    }

    lateinit var thisParent: MainActivity
    lateinit var lsAdapter : ListAdapter
    lateinit var spAdapter: SimpleCursorAdapter
    lateinit var dialog: AlertDialog.Builder
    lateinit var v : View
    var namaKategori : String=""
    var arrKat = ArrayList<String>()
    //   var idMhs : String=""
    lateinit var db : SQLiteDatabase

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        thisParent = activity as MainActivity
        v = inflater.inflate(R.layout.frag_data_warna,container,false)
        db = thisParent.getDbObject()
        dialog = AlertDialog.Builder(thisParent)
        v.btnDeleteWarna.setOnClickListener(this)
        v.btnInsertWarna.setOnClickListener(this)
        v.btnUpdateWarna.setOnClickListener(this)
        v.spinner.onItemSelectedListener = this
//        v.btnCari.setOnClickListener(this)
        v.lsWarna.setOnItemClickListener(itemClick)
        return v
    }

    override fun onStart() {
        super.onStart()
        showDataWarna()
        showDataKat()
    }

//    val itemClick = AdapterView.OnItemClickListener{ parent, view, position, id ->
//        val c: Cursor = parent.adapter.getItem(position) as Cursor
//        v.edNimMhs.setText(c.getColumnIndex("nim"))
//        v.edNamaMhs.setText(c.getColumnIndex("nama"))
//        v.spinner.getItemAtPosition(c.getColumnIndex("nama_prodi"))
//    }

    val itemClick = AdapterView.OnItemClickListener { parent, view, position, id ->
        val c: Cursor = parent.adapter.getItem(position) as Cursor
        v.edKdWarna.setText(c.getString(c.getColumnIndex("_id")))
        v.edNamaWarna.setText(c.getString(c.getColumnIndex("nama_warna")))
        namaKategori = c.getString(c.getColumnIndex("nama_kat"))
        v.spinner.setSelection(getIndex(v.spinner,namaKategori))
    }

    fun getIndex(spinner: Spinner, myString: String): Int {
        var a = spinner.count
        var b : String = ""
        for (i in 0 until a) {
            b = arrKat.get(i)
            if (b.equals(myString, ignoreCase = true)) {
                return i
            }
        }
        return 0
    }

    fun showDataWarna(){
        var sql = "select m.kdwarna as _id, m.nama_warna, k.nama_kat from warna m, kategori k " +
                "where m.id_kat = k.id_kat order by m.nama_warna asc"
//        var sql = "select m.kdmatkul as _id, m.nama_matkul, p.nama_prodi from matkul m, prodi p " +
//                "where m.id_prodi = p.id_prodi order by m.nama_matkul asc"
        val c: Cursor = db.rawQuery(sql, null).also {
            lsAdapter = SimpleCursorAdapter(thisParent,R.layout.item_data_warna, it,
                arrayOf("_id","nama_warna","nama_kat"), intArrayOf(R.id.txKdWarna,R.id.txNamaKategori, R.id.txNamaWarna),
                CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        }
        v.lsWarna.adapter = lsAdapter
    }

    fun showDataKat(){
        val c : Cursor = db.rawQuery("select nama_kat as _id from kategori order by nama_kat asc", null)
        spAdapter = SimpleCursorAdapter(thisParent, android.R.layout.simple_spinner_item,c,
            arrayOf("_id"), intArrayOf(android.R.id.text1),CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        spAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        v.spinner.adapter = spAdapter
        v.spinner.setSelection(0)
        c.moveToFirst()
        var temp : String = ""
        while (!c.isAfterLast()) {
            temp = c.getString(c.getColumnIndex("_id"))
            arrKat.add(temp) //add the item
            c.moveToNext()
        }
    }
    fun insertDataWarna(kdwarna : String , namaWarna: String, id_kat : Int){
        var sql = "insert into warna (kdwarna,nama_warna,id_kat) values (?,?,?)"
        db.execSQL(sql, arrayOf(kdwarna,namaWarna,id_kat))
        showDataWarna()
    }
    val btnInsertDialog = DialogInterface.OnClickListener{ dialog, which ->
        var sql = "select id_kat from kategori where nama_kat ='$namaKategori'"
        val c : Cursor = db.rawQuery(sql, null)
        if(c.count>0){
            c.moveToFirst()
            insertDataWarna(v.edKdWarna.text.toString(), v.edNamaWarna.text.toString(),
                c.getInt(c.getColumnIndex("id_kat")))
            v.edKdWarna.setText("")
            v.edNamaWarna.setText("")
            v.spinner.setSelection(0)
        }
    }

    fun updateDataWarna(kdwarna : String , namaWarna: String, id_kat : Int){
        var cv : ContentValues = ContentValues()
        cv.put("nama_warna",namaWarna)
        cv.put("id_kat",id_kat)
        db.update("warna",cv,"kdwarna = '$kdwarna'",null)
        showDataWarna()
    }
    val btnUpdateDialog = DialogInterface.OnClickListener{ dialog, which ->
        var sql = "select id_kat from kategori where nama_kat ='$namaKategori'"
        val c : Cursor = db.rawQuery(sql, null)
        if(c.count>0){
            c.moveToFirst()
            updateDataWarna(v.edKdWarna.text.toString(), v.edNamaWarna.text.toString(),
                c.getInt(c.getColumnIndex("id_kat")))
            v.edKdWarna.setText("")
            v.edNamaWarna.setText("")
            v.spinner.setSelection(0)
        }
    }
    fun deleteDataWarna(kdwarna: String){
        db.delete("warna","kdwarna = '$kdwarna'",null)
        showDataWarna()
    }
    val btnDeleteDialog = DialogInterface.OnClickListener { dialog, which ->
        deleteDataWarna(v.edKdWarna.text.toString())
        v.edKdWarna.setText("")
        v.edNamaWarna.setText("")
        v.spinner.setSelection(0)
    }



}